<?php
	require_once 'escape_get_post.php';
	session_start();
	if(!isset($_SESSION["master"]) || $_SESSION["master"]==""){
                print '<html><head><script>window.location.href="signin.php";</script></head></html>';
                return;
        }
	if($_SESSION["role"]!="faculty"){
            print '<html><head><script>window.location.href=".";</script></head></html>';
	        return;
	}
	$profile_id=$_SESSION["master"];
	require_once 'connect.php';
?>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<title>Add Paper</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/paperList.css">
<link rel="stylesheet" type="text/css" href="styles/profile.css">
<link rel="stylesheet" type="text/css" href="styles/search_results.css">
<link rel="stylesheet" type="text/css" href="styles/edit_profile.css">
<link rel="stylesheet" type="text/css" href="styles/add_paper.css">
<link rel="stylesheet" type="text/css" href="styles/signIn.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
<script>
add_author=function(){
	//document.getElementById("author_body").innerHTML+='<tr>\n';
    var child_row = document.createElement("tr");
    document.getElementById("author_body").appendChild(child_row);
    child_row.innerHTML='<td><input class="table_edit_textbox author_name_box" type="text"></td>\n<td>&nbsp;<input class="table_edit_textbox author_user_box" type="text"></td>\n<td><div class="minus_sign" onclick="remove_author(this);">-</div></td>';
}
remove_author=function(obj){
	$(obj).parent().parent().remove();
}
add_paper=function(force=false){
    $("#signin_response").html("");
    title=$("#add_title").val().trim();
    if(title==""){
        $("#signin_response").html("Can't leave title empty");
        return;
    }
    author_names="";
    author_error=false;
    $(".author_name_box").each(function(index){
        if($(this).val().trim()==""){
            author_error=true;
        }else{
            author_names+=$(this).val().trim()+"|";
        }
    });
    author_names=author_names.replace(/\|$/,"");
    if(author_error){
        $("#signin_response").html("One of the author names left empty. Delete the field if not needed. One is compulsory");
        return;
    }
    author_users="";
    $(".author_user_box").each(function(index){
        author_users+=$(this).val().trim()+"|";
    });
    author_users=author_users.replace(/\|$/,"");
    year=$("#add_year").val().trim();
    if(year==""){
        $("#signin_response").html("Can't leave date of issue empty");
        return;
    }
    publisher=$("#add_publisher").val().trim();
    if(publisher==""){
        $("#signin_response").html("Can't leave conference/journal name empty");
        return;
    }
    publisher_type=$("#add_publisher_type").val();
    
    research_areas="";
    
    $("#add_research_area > .research_area_list_entry > input").each(function(){
        if(this.checked == true){
            research_areas += this.id + "|";
        }
    });
    research_areas = research_areas.replace(/\|$/,"");
    
    abstract=$("#add_abstract").val();
    data={"title":title, "author_names":author_names, "author_users":author_users,"year":year,"publisher":publisher,"publisher_type":publisher_type, "research_area":research_areas,"abstract":abstract};
    if(force)
        data["force_insert"]="true";
    $.post("backend/?method=5", data).done(function(resp){
        if(resp["code"]==301){
            $("#signin_response").html('<div class="h_separator"></div>The title you entered conflicts with some other publication in the database. Are you sure you still want to insert?<div class="submit_button" onclick="add_paper(true);">Yes</div> <div class="submit_button" onclick="cancel_insert();">Cancel</div><br><div class="h_separator"></div>');
        }else if(resp["code"]!=200){
            $("#signin_response").html(resp["message"]);
        }else{
            window.location.href="pub.php?p_id="+resp["p_id"];
        }
    });
}
cancel_insert=function(){
    window.location.href="add_paper.php";
}
bibtex_upload=function(){
    $("#upload_bibtex_okay").html("");
    $("#upload_bibtex_error").html("");
    form_data = new FormData($("#upload_bibtex_form")[0]);
    $.ajax({
        "url" : "backend/?method=6&ignore_conflicts="+$(".conflict:checked").val(),
        type: "POST",
        data : form_data,
        processData: false,
    contentType: false}).done(function(resp){
        if(resp["code"]==201){
            window.location.href="resolve_conflicts.php";
        }else if(resp["code"]!=200){
            $("#upload_bibtex_error").html(resp["message"]);
        }else{
            $("#upload_bibtex_okay").html("The BibTex entries have been uploaded. Please visit the publication pages (your profile page) and add the abstract.")
        }
        $("#upload_bibtex_button").html("Upload");
    });
    $("#upload_bibtex_button").html("Uploading...");
}
bibtex_upload_text=function(){
    $("#upload_bibtex_okay").html("");
    $("#upload_bibtex_error").html("");
    bibtex_content = $("#add_bibtex").val().trim();
    console.log(bibtex_content);
    data = {"bibtex_content" : bibtex_content};
    $.post("backend/?method=11&ignore_conflicts="+$(".conflict:checked").val()+"&bibtex_content="+bibtex_content).done(function(resp){
        if(resp["code"]==201){
            window.location.href="resolve_conflicts.php";
        }else if(resp["code"]!=200){
            $("#upload_bibtex_error").html(resp["message"]);
        }else{
            $("#upload_bibtex_okay").html("The BibTex entries have been uploaded. Please visit the publication pages (your profile page) and add the abstract.")
        }
        $("#upload_bibtexentry_button").html("Submit");
    });
    $("#upload_bibtexentry_button").html("Submitting");
}
</script>

</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
		<?php require_once 'title.php'; ?>
		<div id="the_body">
            <div class="body_divs edit_box">

                <div class="list_header">
                    <div class="publications" >
                        Add Paper Entry
                    </div>
                </div>

                <div class="h_separator"></div>
                <div class="edit_profile_details">
                    
                    <div class="paper_box_entry">
                        Title: <input class="form_textbox" type="text" id="add_title">
                        <div class="description"> Enter the title of the paper.</div>
                    </div>
                    
                    <div class="paper_box_entry">
                        Authors:                        
                        <table cellpadding="0" class="author_table">
                          <tbody id="author_body">  
            			    <tr>
                                <td>Name</td>
                                <td>&nbsp;ID</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><input class="table_edit_textbox author_name_box" type="text"></td>
                                <td>&nbsp;<input class="table_edit_textbox author_user_box" type="text"></td>
                                <td></td>
                            </tr>

			              </tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><div class="plus_sign" onclick="add_author();">+</div></td>
                            </tr>
                        </table>
                        
                        <div class="description">
                            ID is optional, it is used to link profiles<br>
                            If author is a student his ID is his roll no.<br>
                            If he is a professor his ID is his email id without "@iith.ac.in" suffix. <br>
                            For eg: if the email id is "cse_prof@iith.ac.in" then ID is "cse_prof".
                        </div>
                    </div>
                    
                    <div class="paper_box_entry">
                        Date of Issue: <input class="edit_textbox" type="text" id="add_year">
                        <div class="description"> Enter the year the paper was published.</div>
                    </div>
                    
                    <div class="paper_box_entry">
                        Conference/Journal Name: <input class="form_textbox" type="text" id="add_publisher">
                        <div class="description"> Enter the name of the journal/conference the paper was published in.</div>
                    </div>
                    <div class="paper_box_entry">
                        Publication type: 
                        <select class = "select_menu" id="add_publisher_type">
                            <option value="journal">Journal</option>
                            <option value="series">Conference</option>
                            <option value="booktitle">Book</option>
                        </select>
                    </div>
                    <div class="edit_research_areas">
                        Select Research Areas:
                        <!--
                        <select class = "select_menu" id="add_research_area">
                            <?php
                                $result=mysql_query("select area_name, r_id from research_areas;");
                                for($i=0;$i<mysql_num_rows($result);$i++){ 
                                $row=mysql_fetch_row($result);
                            ?>
                            <option value="<?php print $row[1]; ?>"><?php print $row[0]; ?></option>
                            <?php }?>	
            			</select>
                        -->
                        <div class="dropdown" style="margin-left:20px; font-size:16px;">
                            <div>
                                <div class="research_area_box">Research Areas
                                    <div class="arrow_down"></div>
                                </div>
                            </div>
                            <div class="research_area_checklist" id="add_research_area">
                                <?php 
                                    $result=mysql_query("select area_name, r_id from research_areas;");
                                    for($i=0;$i<mysql_num_rows($result);$i++){ 
                                        $row=mysql_fetch_row($result);
                                ?>
                                <div class="research_area_list_entry">
                                    <input type="checkbox" id="<?php print $row[1]; ?>" >
                                    <label for="<?php print $row[1]; ?>" class="box_label"><?php print $row[0]; ?></label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        
                        <div class="description"> Choose a research area by selecting from the dropdown.</div>
                    </div>
                    <div class="edit_website">
                        <div style="float:left;">
                            Abstract :
                        </div>
                        <textarea class="website_box" cols="50" rows="10" id="add_abstract" style="max-width:80%;"></textarea>
                        <div class="description">Paste the abstract of the paper in the above box.</div>
                    </div>
                    <div id="signin_response" style="text-align:left"></div>
                    <div class="submit_button" onclick="add_paper();">
							Save
                    </div>
                </div>
            </div>
            <div class="body_divs upload_bibtex_box">
                <div class="list_header">
                    <div class="publications" >
                        Bibtex
                    </div>
                </div>
                <div class="h_separator"></div> 
                <div style="margin-top: 10px; font-size: 14px;">
                        <input type="radio" class="conflict" name="conflict" value="true" checked> Ignore conflicts<br>
                        <input type="radio" class="conflict" name="conflict" value="false"> Resolve conflicts manually<br>
                </div>
                <div class="list_header">
                    <div class="publications" >
                        Enter Bibtex
                    </div>
                </div>
                <div class="h_separator"></div><br> 
                
                <textarea class="bibtex_content" cols="50" rows="10" id="add_bibtex" placeholder="Paste bibtex here"></textarea>
                <div class="description"> 
                    You can input multiple bibtex entries.
                </div>
                <div class="submit_button" onclick="bibtex_upload_text();" id="upload_bibtexentry_button">Submit</div>
                
                <div class="list_header">
                    <div class="publications" >
                        Upload Bibtex File
                    </div>
                </div>
                <div class="h_separator"></div><br>
                
                <form id="upload_bibtex_form"><input class="choose_file" type="file" name="bibtex"/></form>
                <div class="description"> 
                    Choose a single bibtex file (.bib). It can contain entries for multiple papers.
                </div>
                
                <div class="submit_button" onclick="bibtex_upload();" id="upload_bibtex_button">Upload</div>

                <div class="paper_box_entry">
                    <div class="okay_message" id="upload_bibtex_okay"></div>
                    <div class="error_message" id="upload_bibtex_error"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html> 
