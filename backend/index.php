<?php
	require_once '../escape_get_post.php';
	require_once '../ldap-auth.php';
	set_time_limit(0);
	function get_auth($username, $password){
		require_once '../ldap-conf.php';	
		$response=array();
		$auth = ldap_auth_iith($ldap_domain, $username, $password);
		if ($auth["success"]){
			$response["authentication"]=true;
			$response["role"]=$auth["role"];
			$result = mysql_query("select name from user_profiles where username='$username';");
			if(mysql_num_rows($result)==0){
				$name = $auth["name"];
				mysql_query("insert into user_profiles values('$username','$name', '', '');");
				copy("../images/user.png", "../images/profile_images/" . $username . ".png");
				$response["first_login"]="true";
				$response["name"]=$name;
			}else{	
				$response["first_login"]="false";
				$row = mysql_fetch_row($result);
				$response["name"]=$row[0];
			}
		}else{
			$response["authentication"]=false;
		}
		return $response;
	}
	header('Content-Type: application/json');
	session_start();
	if (!isset($_GET["method"]) || $_GET["method"]==""){
		print '{"code":401, "message":"method missing"}';
		return;
	}
	$code = $_GET["method"];

	/* 
		1 status
		2 sign in
		3 sign out
		4 edit_profile
		5 add_paper
		6 add_bibtex
		7 edit_paper
		8 upload_image
	*/
	
	if($code == 1){
		print '{"code":200}';
		return;
	}else if($code == 2){
		if(!isset($_POST["username"]) || $_POST["username"]==""){
			print '{"code":401, "message":"username missing"}';
			return;
		}
		if(!isset($_POST["password"]) || $_POST["password"]==""){
			print '{"code":402, "message":"password missing"}';
			return;
		}
		if(!isset($_POST["captcha-response"]) || $_POST["captcha-response"]==""){
			print '{"code":403, "message":"Please fill captcha"}';
			return;
		}
		$username = $_POST["username"];
		$password = $_POST["password"];
		$captcha = $_POST["captcha-response"];
		require_once "../lib/recaptchalib.php";
		// secret key
		$secret = "6LfU-CITAAAAALT-iua4a9F9TxQlF8mGBFUDA3Bo";
		$response = null;
		// check secret key
		$reCaptcha = new ReCaptcha($secret);
		$response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"],$captcha);
		if (!($response != null && $response->success)) {
			print '{"code":404, "message":"Failed captcha verification"}';
			return;
		}
		$user_info = get_auth($username, $password);
		if ($user_info["authentication"]==false){
			print '{"code":301, "message":"Invalid username/password"}';
			return;
		}
		print '{"code":200, "name":"'.$user_info["name"].'", "first_login":'.$user_info["first_login"].'}';
		$_SESSION["master"]=$username;
		$_SESSION["name"]=$user_info["name"];
		$_SESSION["role"]=$user_info["role"];
		$_SESSION["conflict_entries"]=array();
		return;
	}else if ($code == 3){
		$_SESSION["master"]="";
		echo '{"code":200}';
		return;
	}else if ($code == 4){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]==""){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_GET["name"]) || trim($_GET["name"])==""){
			echo '{"code":401, "message":"Name missing"}';
			return;
		}
		if(!isset($_GET["research_areas"])){
			echo '{"code":402, "message":"Research areas not set"}';
			return;
		}
		if(!isset($_GET["websites"])){
			echo '{"code":403, "message":"Websites not set"}';
			return;
		}
		$name = mysql_real_escape_string($_GET["name"]);
		$research_areas = array_filter(explode("|", $_GET["research_areas"]));
		$websites = mysql_real_escape_string(implode(",",array_filter(array_map('trim', explode(",", $_GET["websites"])))));

        $description = mysql_real_escape_string($_GET["description"]);
		$result = mysql_query("update user_profiles set name='$name', websites='$websites', description='$description' where username='$username';");
		if (!$result){
			echo '{"code": 302, "Error updating name/websites"}';
			return;
		}
		$_SESSION["name"]=$name;
		$result = mysql_query("delete from user_research_area_mapping where username='$username';");
		if (count($research_areas)!=0){
			$query="insert into user_research_area_mapping values";
			for ($i=0;$i<count($research_areas);$i++){
				$query.="('','$username', '{$research_areas[$i]}')";
				if ($i!=count($research_areas)-1)
					$query.=",";
			}
			$query.=";";
			$result =mysql_query($query);
			if (!$result){
				echo '{"code": 303, "message":"Error updating research areas. Check for foreign key constraints. All research areas removed"}';
				return;
			}
		}
		echo '{"code":200}';
	}else if($code==5){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]=="" || $_SESSION["role"]!="faculty"){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_POST["title"]) || trim($_POST["title"])==""){
			echo '{"code":401, "message":"Title missing"}';
			return;
		}
		$title =$_POST["title"];
		if(!isset($_POST["author_names"]) || $_POST["author_names"]==""){
			echo '{"code":402, "message":"Author name missing"}';
			return;
		}
		$author_names = explode("|",$_POST["author_names"]);
		for($i=0;$i<count($author_names);$i++){
			if (trim($author_names[$i])==""){
				echo '{"code":402, "message":"Author name missing"}';
				return;
			}
		}
		if(!isset($_POST["author_users"])){
			echo '{"code":403, "message":"Author usernames missing"}';
			return;
		}
		$author_users=explode("|",$_POST["author_users"]);

		if(count($author_names)!=count($author_users)){
			echo '{"code":404, "message":"Author name and user count dont match"}';
			return;
		}
		if(!isset($_POST["year"]) || $_POST["year"]==""){
			echo '{"code":405, "message":"Year of issue missing"}';
			return;
		}
		$year=intval($_POST["year"]);
		if(!isset($_POST["publisher"]) || $_POST["publisher"]==""){
			echo '{"code":406, "message":"Publisher missing"}';
			return;
		}
		$publisher=$_POST["publisher"];
		if(!isset($_POST["publisher_type"]) || $_POST["publisher_type"]==""){
			echo '{"code": 409, "message":"Publisher type missing"}';
		}
		$publisher_type=$_POST["publisher_type"];
		$valid_types=array("journal", "series", "booktitle");
		if(!in_array($publisher_type, $valid_types)){
			echo '{"code":303, "message":"Invalid publisher type"}';
			return;
		}
		if(!isset($_POST["research_area"]) || $_POST["research_area"]==""){
			echo '{"code":407, "message":"Research area missing"}';
			return;
		}
        
		$research_area=$_POST["research_area"];
        $research_areas = explode("|",$_POST["research_area"]);
        
        
		if(!isset($_POST["abstract"])){
			echo '{"code":408, "message":"Abstract missing"}';
			return;
		}
		$abstract=$_POST["abstract"];
		$force=false;
		if(isset($_POST["force_insert"]) && $_POST["force_insert"]=="true"){
			$force=true;
		}
		if($force==false){
			$result=mysql_query("select p_id from publications where title='$title';");
			if(mysql_num_rows($result)!=0){
				echo '{"code":301, "message":"Title conflict"}';
				return;
			}
		}
		
		$non_empty=array_values(array_filter($author_users));
		if(count($non_empty)!=0){
			$query="select id from (";
			for ($i=0;$i<count($non_empty);$i++){
				$query.="select $i as id, binary('{$non_empty[$i]}') as username ";
				if($i!=count($non_empty)-1){
					$query.=" union all ";
				}
			}
			$query.=") as A where username not in (select distinct username from user_profiles);";
			$result=mysql_query($query);
			if(mysql_num_rows($result)!=0){
				$error = "Usernames - ";
				for ($i=0;$i<mysql_num_rows($result);$i++){
					$row=mysql_fetch_row($result);
					$error .= $non_empty[intval($row[0])]." ";
				}
				$error .= "donot exist";
				echo '{"code":302, "message":"'.$error.'"}';
				return;
			}
		}
		mysql_query("insert into publications values('','$username','$title','$publisher','$publisher_type', '$year','$abstract');");
		$last_id=mysql_insert_id();
        
        $query="INSERT INTO publication_research_area_mapping VALUES ";
        for($i=0; $i<count($research_areas); $i++){
            $query .= "('$last_id','$research_areas[$i]','')";
            if($i!=count($research_areas)-1)
				$query .=",";
        }
        $query .= ";";
        mysql_query($query);
        
		$query="insert into authors values ";
		for($i=0;$i<count($author_names);$i++){
			$aname=trim($author_names[$i]);
			$auser=trim($author_users[$i]);
			$j=$i+1;
			$query .= "('$last_id','$j','{$aname}', '{$auser}') ";
			if($i!=count($author_names)-1)
				$query .=",";
		}
		$query.=";";
		mysql_query($query);
		echo '{"code":200,"p_id":'.$last_id.'}';
	}else if($code==6){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]=="" || $_SESSION["role"]!="faculty"){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_FILES["bibtex"]) || $_FILES["bibtex"]["error"]!=0){
			echo '{"code":401, "message":"File missing. Please choose a bibtex file from above."}';
			return;
		}
		if(!isset($_GET["ignore_conflicts"]) || ($_GET["ignore_conflicts"]!="true" and $_GET["ignore_conflicts"]!="false")){
			echo '{"code":402, "message":"Missing valid ignore-flag"}';
			return;
		}
		$ignore_conflicts=$_GET["ignore_conflicts"];
		//print_r($_GET);
		//print_r($_FILES);
		$_GET['library']=1;
		require_once '../lib/bibtexbrowser.php';
		$tmp_name = $_FILES["bibtex"]["tmp_name"];
		$bb = new BibDataBase();
		$bb -> load($tmp_name);
		$entries=$bb->multisearch(array('title'=>'.*'));
		$_SESSION["conflict_entries"]=array();
		$conflict_flag=false;
		foreach ($entries as $bibentry) { 
 			$fields = $bibentry->fields;
 			if(!isset($fields["title"]) || !isset($fields["year"]) || !isset($fields["author"])){
 				continue;
 			}
 			$title = mysql_real_escape_string($fields["title"]);
 			$authors = explode(" and ", mysql_real_escape_string($fields["author"]));
 			$year = mysql_real_escape_string($fields["year"]);
 			unset($fields["title"]);
 			unset($fields["author"]);
 			unset($fields["year"]);
 			if(isset($fields["_author"]))
 				unset($fields["_author"]);
 			if(isset($fields["journal"])){
 				$publisher=mysql_real_escape_string($fields["journal"]);
 				unset($fields["journal"]);
 				$publisher_type = "journal";
 			}else if(isset($fields["series"])){
 				$publisher=mysql_real_escape_string($fields["series"]);
 				unset($fields["series"]);
 				$publisher_type = "series";
 			}else if(isset($fields["booktitle"])){
 				$publisher=mysql_real_escape_string($fields["booktitle"]);
 				unset($fields["booktitle"]);
 				$publisher_type = "booktitle";
 			}else{
 				continue;
 			}
			$result = mysql_query("select p_id from publications where
			 title='$title';");
			/*if(!$result){
				echo "select p_id from publications where title='$title';";
			}*/
			if(mysql_num_rows($result)!=0){
				if($ignore_conflicts=="false"){
					$row=mysql_fetch_row($result);
					$conflict_flag=true;
					$bibentry->fields["_conflict_id"]=$row[0];
					array_push($_SESSION["conflict_entries"],$bibentry->fields);
				}
				continue;
			} 			
			mysql_query("insert into publications values('','$username','$title','$publisher','$publisher_type','$year','0');");
			$last_id=mysql_insert_id();
			$query="insert into authors values ";
			for($i=0;$i<count($authors);$i++){
				$aname=trim($authors[$i]);
				$auser="";
				$j=$i+1;
				$query .= "('$last_id','$j','{$aname}', '{$auser}') ";
				if($i!=count($authors)-1)
					$query .=",";
			}
			$query.=";";
			mysql_query($query);
			if(count($fields)!=0){
				$query="insert into bibtex_entries values ";
				$i=0;
				foreach($fields as $key => $value){
					$okey=mysql_real_escape_string($key);
					$ovalue=mysql_real_escape_string($value);
					$query .= "('','$last_id','$okey','$ovalue') ";
					if($i!=count($fields)-1)
						$query .=",";
					$i++;
				}
				$query.=";";
				mysql_query($query);
			}
		}
		if (!$conflict_flag)
			echo '{"code":200}';
		else
			echo '{"code":201}';
	}else if($code==7){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]=="" || $_SESSION["role"]!="faculty"){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_POST["p_id"]) || $_POST["p_id"]==""){
			echo '{"code":410, "message":"p_id missing"}';
			return;
		}
		$p_id=$_POST["p_id"];
		$result=mysql_query("select p_id from publications where p_id='$p_id' and owner='$username';");
		if(mysql_num_rows($result)==0) {
			echo '{"code":304, "message":"Access violation"}';
			return;
		}
		if(!isset($_POST["title"]) || trim($_POST["title"])==""){
			echo '{"code":401, "message":"Title missing"}';
			return;
		}
		$title =$_POST["title"];
		if(!isset($_POST["author_names"]) || $_POST["author_names"]==""){
			echo '{"code":402, "message":"Author name missing"}';
			return;
		}
		$author_names = explode("|",$_POST["author_names"]);
		for($i=0;$i<count($author_names);$i++){
			if (trim($author_names[$i])==""){
				echo '{"code":402, "message":"Author name missing"}';
				return;
			}
		}
		if(!isset($_POST["author_users"])){
			echo '{"code":403, "message":"Author usernames missing"}';
			return;
		}
		$author_users=explode("|",$_POST["author_users"]);

		if(count($author_names)!=count($author_users)){
			echo '{"code":404, "message":"Author name and user count dont match"}';
			return;
		}
		if(!isset($_POST["year"]) || $_POST["year"]==""){
			echo '{"code":405, "message":"Year of issue missing"}';
			return;
		}
		$year=intval($_POST["year"]);
		if(!isset($_POST["publisher"]) || $_POST["publisher"]==""){
			echo '{"code":406, "message":"Publisher missing"}';
			return;
		}
		$publisher=$_POST["publisher"];
		if(!isset($_POST["publisher_type"]) || $_POST["publisher_type"]==""){
			echo '{"code": 409, "message":"Publisher type missing"}';
		}
		$publisher_type=$_POST["publisher_type"];
		$valid_types=array("journal", "series", "booktitle");
		if(!in_array($publisher_type, $valid_types)){
			echo '{"code":303, "message":"Invalid publisher type"}';
			return;
		}
		if(!isset($_POST["research_area"]) || $_POST["research_area"]==""){
			echo '{"code":407, "message":"Research area missing"}';
			return;
		}
		$research_area = $_POST["research_area"];
		$research_areas = explode("|",$_POST["research_area"]);
        
		if(!isset($_POST["abstract"])){
			echo '{"code":408, "message":"Abstract missing"}';
			return;
		}
		$abstract=$_POST["abstract"];
		$force=false;
		if(isset($_POST["force_insert"]) && $_POST["force_insert"]=="true"){
			$force=true;
		}
		if($force==false){
			$result=mysql_query("select p_id from publications where title='$title' and p_id!='$p_id';");
			if(mysql_num_rows($result)!=0){
				echo '{"code":301, "message":"Title conflict"}';
				return;
			}
		}
		
		$non_empty=array_values(array_filter($author_users));
		if(count($non_empty)!=0){
			$query="select id from (";
			for ($i=0;$i<count($non_empty);$i++){
				$query.="select $i as id, binary('{$non_empty[$i]}') as username ";
				if($i!=count($non_empty)-1){
					$query.=" union all ";
				}
			}
			$query.=") as A where username not in (select distinct username from user_profiles);";
			$result=mysql_query($query);
			if(mysql_num_rows($result)!=0){
				$error = "Usernames - ";
				for ($i=0;$i<mysql_num_rows($result);$i++){
					$row=mysql_fetch_row($result);
					$error .= $non_empty[intval($row[0])]." ";
				}
				$error .= "donot exist";
				echo '{"code":302, "message":"'.$error.'"}';
				return;
			}
		}
		//mysql_query("insert into publications values('','$username','$title','$publisher','$publisher_type', '$year','$research_area','$abstract');");
		mysql_query("update publications set title='$title', publisher='$publisher', publisher_type='$publisher_type', year='$year', abstract='$abstract' where p_id='$p_id';");
		$last_id=intval($p_id);
        
        mysql_query("delete from publication_research_area_mapping where p_id='$p_id';");
        $query="INSERT INTO publication_research_area_mapping VALUES ";
        for($i=0; $i<count($research_areas); $i++){
            $query .= "('$last_id','$research_areas[$i]','')";
            if($i!=count($research_areas)-1)
                $query .=",";
        }
        $query .= ";";
        mysql_query($query);
        
		mysql_query("delete from authors where p_id='$p_id';");
		$query="insert into authors values ";
		for($i=0;$i<count($author_names);$i++){
			$aname=trim($author_names[$i]);
			$auser=trim($author_users[$i]);
			$j=$i+1;
			$query .= "('$last_id','$j','{$aname}', '{$auser}') ";
			if($i!=count($author_names)-1)
				$query .=",";
		}
		$query.=";";
		mysql_query($query);
		echo '{"code":200,"p_id":'.$last_id.'}';
	}else if($code==8){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]==""){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_FILES["profile_image"]) || $_FILES["profile_image"]["error"]!=0){
			echo '{"code":401, "message":"File missing. Please choose an image file from above."}';
			return;
		}
		$tmp_name = $_FILES["profile_image"]["tmp_name"];
		move_uploaded_file($tmp_name, "../images/profile_images/" . $username . ".png");
		echo '{"code":200}';
	}else if($code==9){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]=="" || $_SESSION["role"]!="faculty"){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_POST["choice_string"])||$_POST["choice_string"]==""){
			echo '{"code":401, "message":"Choice string missing"}';
			return;
		}
		$choices=explode('|',$_POST["choice_string"]);
		if(count($choices)!=count($_SESSION["conflict_entries"])){
			echo '{"code":301, "message":"Choice count does not match conflict count"}';
			return;
		}
		for($i=0;$i<count($choices);$i++){
			$fields = $_SESSION["conflict_entries"][$i];
			unset($fields["_conflict_id"]);
			$title = mysql_real_escape_string($fields["title"]);
 			$authors = explode(" and ", mysql_real_escape_string($fields["author"]));
 			$year = mysql_real_escape_string($fields["year"]);
 			unset($fields["title"]);
 			unset($fields["author"]);
 			unset($fields["year"]);
 			if(isset($fields["_author"]))
 				unset($fields["_author"]);
 			if(isset($fields["journal"])){
 				$publisher=mysql_real_escape_string($fields["journal"]);
 				unset($fields["journal"]);
 				$publisher_type = "journal";
 			}else if(isset($fields["series"])){
 				$publisher=mysql_real_escape_string($fields["series"]);
 				unset($fields["series"]);
 				$publisher_type = "series";
 			}else if(isset($fields["booktitle"])){
 				$publisher=mysql_real_escape_string($fields["booktitle"]);
 				unset($fields["booktitle"]);
 				$publisher_type = "booktitle";
 			}else{
 				continue;
 			}
 			if($choices[$i]=="true"){
 				mysql_query("insert into publications values('','$username','$title','$publisher','$publisher_type','$year','0');");
				$last_id=mysql_insert_id();
				$query="insert into authors values ";
				for($i=0;$i<count($authors);$i++){
					$aname=trim($authors[$i]);
					$auser="";
					$j=$i+1;
					$query .= "('$last_id','$j','{$aname}', '{$auser}') ";
					if($i!=count($authors)-1)
						$query .=",";
				}
				$query.=";";
				mysql_query($query);
				if(count($fields)!=0){
					$query="insert into bibtex_entries values ";
					$i=0;
					foreach($fields as $key => $value){
						$okey=mysql_real_escape_string($key);
						$ovalue=mysql_real_escape_string($value);
						$query .= "('','$last_id','$okey','$ovalue') ";
						if($i!=count($fields)-1)
							$query .=",";
						$i++;
					}
					$query.=";";
					mysql_query($query);
				}
			}
		}
		$_SESSION["conflict_entries"]=array();
		echo '{"code":200}';
	}elseif($code==10){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]=="" || $_SESSION["role"]!="faculty"){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_POST["p_id"]) || $_POST["p_id"]==""){
			echo '{"code":410, "message":"p_id missing"}';
			return;
		}
		$p_id=$_POST["p_id"];
		$result=mysql_query("select p_id from publications where p_id='$p_id' and owner='$username';");
		if(mysql_num_rows($result)==0) {
			echo '{"code":304, "message":"Access violation"}';
			return;
		}
		mysql_query("delete from publications where p_id='$p_id';");
		mysql_query("delete from authors where p_id='$p_id';");
		mysql_query("delete from bibtex_entries where p_id='$p_id';");
		mysql_query("delete from publication_research_area_mapping where p_id='$p_id';");
		echo '{"code":200}';
	}else if($code==11){
		if(!isset($_SESSION["master"]) || $_SESSION["master"]=="" || $_SESSION["role"]!="faculty"){
			echo '{"code":301, "message":"Not logged in"}';	
			return;
		}
		$username = $_SESSION["master"];
		if(!isset($_GET["bibtex_content"])){
			echo '{"code":401, "message":"Bibtex box empty. Please enter a valid bibtex entry."}';
			return;
		}
		if(!isset($_GET["ignore_conflicts"]) || ($_GET["ignore_conflicts"]!="true" and $_GET["ignore_conflicts"]!="false")){
			echo '{"code":402, "message":"Missing valid ignore-flag"}';
			return;
		}
		$ignore_conflicts=$_GET["ignore_conflicts"];

		$_GET['library']=1;
		require_once '../lib/bibtexbrowser.php';
		$content = $_GET["bibtex_content"];

		// Create bibdatabase form string
		$data = fopen('php://memory','x+');
		fwrite($data, $content);
		fseek($data, 0);
		$bb = new BibDataBase();
		$bb->update_internal("inline", $data);

		$entries=$bb->multisearch(array('title'=>'.*'));
		$_SESSION["conflict_entries"]=array();
		$conflict_flag=false;
		foreach ($entries as $bibentry) { 
 			$fields = $bibentry->fields;
 			if(!isset($fields["title"]) || !isset($fields["year"]) || !isset($fields["author"])){
 				continue;
 			}
 			$title = mysql_real_escape_string($fields["title"]);
 			$authors = explode(" and ", mysql_real_escape_string($fields["author"]));
 			$year = mysql_real_escape_string($fields["year"]);
 			unset($fields["title"]);
 			unset($fields["author"]);
 			unset($fields["year"]);
 			if(isset($fields["_author"]))
 				unset($fields["_author"]);
 			if(isset($fields["journal"])){
 				$publisher=mysql_real_escape_string($fields["journal"]);
 				unset($fields["journal"]);
 				$publisher_type = "journal";
 			}else if(isset($fields["series"])){
 				$publisher=mysql_real_escape_string($fields["series"]);
 				unset($fields["series"]);
 				$publisher_type = "series";
 			}else if(isset($fields["booktitle"])){
 				$publisher=mysql_real_escape_string($fields["booktitle"]);
 				unset($fields["booktitle"]);
 				$publisher_type = "booktitle";
 			}else{
 				continue;
 			}
			$result = mysql_query("select p_id from publications where
			 title='$title';");
			/*if(!$result){
				echo "select p_id from publications where title='$title';";
			}*/
			if(mysql_num_rows($result)!=0){
				if($ignore_conflicts=="false"){
					$row=mysql_fetch_row($result);
					$conflict_flag=true;
					$bibentry->fields["_conflict_id"]=$row[0];
					array_push($_SESSION["conflict_entries"],$bibentry->fields);
				}
				continue;
			} 			
			mysql_query("insert into publications values('','$username','$title','$publisher','$publisher_type','$year','0');");
			$last_id=mysql_insert_id();
			$query="insert into authors values ";
			for($i=0;$i<count($authors);$i++){
				$aname=trim($authors[$i]);
				$auser="";
				$j=$i+1;
				$query .= "('$last_id','$j','{$aname}', '{$auser}') ";
				if($i!=count($authors)-1)
					$query .=",";
			}
			$query.=";";
			mysql_query($query);
			if(count($fields)!=0){
				$query="insert into bibtex_entries values ";
				$i=0;
				foreach($fields as $key => $value){
					$okey=mysql_real_escape_string($key);
					$ovalue=mysql_real_escape_string($value);
					$query .= "('','$last_id','$okey','$ovalue') ";
					if($i!=count($fields)-1)
						$query .=",";
					$i++;
				}
				$query.=";";
				mysql_query($query);
			}
		}
		if (!$conflict_flag)
			echo '{"code":200}';
		else
			echo '{"code":201}';
	}else{
		echo '{"code":402,"message":"Invalid method"}';
	}
?>

