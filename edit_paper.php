<?php
	require_once 'escape_get_post.php';
	session_start();
	if(!isset($_SESSION["master"]) || $_SESSION["master"]==""){
                print '<html><head><script>window.location.href="signin.php";</script></head></html>';
                return;
    }
    if($_SESSION["role"]!="faculty" || !isset($_GET["p_id"]) || $_GET["p_id"]==""){
        print '<html><head><script>window.location.href=".";</script></head></html>';
        return;
    }
    $profile_id=$_SESSION["master"];
    $p_id = $_GET["p_id"];
	require_once 'connect.php';
    $result = mysql_query(" SELECT title, 
                                   year, 
                                   publisher, 
                                   publisher_type,
                                   abstract, 
                                   Group_concat(name ORDER BY author_order SEPARATOR '|'), 
                                   Group_concat(username ORDER BY author_order SEPARATOR '|')
                            FROM   publications AS A 
                            JOIN   authors AS B 
                            ON     A.p_id = B.p_id 
                            AND    A.p_id = '$p_id' 
                            AND    owner = '$profile_id' 
                            GROUP  BY A.p_id; ");
    if(mysql_num_rows($result)==0){
        print '<html><head><script>window.location.href=".";</script></head></html>';
        return;
    }
    $row = mysql_fetch_row($result);
    $title = $row[0];
    $year = $row[1];
    $publisher = $row[2];
    $publisher_type = $row[3];
    $abstract = $row[4];
    $author_names = explode("|",$row[5]);
    $author_count = count($author_names);
    $author_users = explode("|",$row[6]);

    $result = mysql_query(" SELECT r_id
                            FROM   publication_research_area_mapping
                            WHERE  p_id = '$p_id'");
    $selected_rids = array();
    while($row = mysql_fetch_row($result)){
        $selected_rids[] = $row[0];
    }
?>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<title><?php print $title; ?></title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/paperList.css">
<link rel="stylesheet" type="text/css" href="styles/profile.css">
<link rel="stylesheet" type="text/css" href="styles/search_results.css">
<link rel="stylesheet" type="text/css" href="styles/edit_profile.css">
<link rel="stylesheet" type="text/css" href="styles/add_paper.css">
<link rel="stylesheet" type="text/css" href="styles/signIn.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
<script>
add_author=function(){
	//document.getElementById("author_body").innerHTML+='<tr>\n';
    var child_row = document.createElement("tr");
    document.getElementById("author_body").appendChild(child_row);
    child_row.innerHTML='<td><input class="table_edit_textbox author_name_box" type="text"></td>\n<td>&nbsp;<input class="table_edit_textbox author_user_box" type="text"></td>\n<td><div class="minus_sign" onclick="remove_author(this);">-</div></td>';
}
remove_author=function(obj){
	$(obj).parent().parent().remove();
}
delete_paper=function(){
    $("#signin_response").html("");
    if(!confirm("Are you sure you want to delete this entry?")){
        return;
    }
    $.post("backend/?method=10", {"p_id":"<?php print $p_id; ?>"}).done(function(resp){
        if(resp["code"]==200){
            window.location.href=".";
        }else{
            $("#signin_response").html(resp["message"]);
        }
    });
}
add_paper=function(force=false){
    $("#signin_response").html("");
    title=$("#add_title").val().trim();
    if(title==""){
        $("#signin_response").html("Can't leave title empty");
        return;
    }
    author_names="";
    author_error=false;
    $(".author_name_box").each(function(index){
        if($(this).val().trim()==""){
            author_error=true;
        }else{
            author_names+=$(this).val().trim()+"|";
        }
    });
    author_names=author_names.replace(/\|$/,"");
    if(author_error){
        $("#signin_response").html("One of the author names left empty. Delete the field if not needed. One is compulsory");
        return;
    }
    author_users="";
    $(".author_user_box").each(function(index){
        author_users+=$(this).val().trim()+"|";
    });
    author_users=author_users.replace(/\|$/,"");
    year=$("#add_year").val().trim();
    if(year==""){
        $("#signin_response").html("Can't leave date of issue empty");
        return;
    }
    publisher=$("#add_publisher").val().trim();
    if(publisher==""){
        $("#signin_response").html("Can't leave conference/journal name empty");
        return;
    }
    publisher_type=$("#add_publisher_type").val();
    
    research_areas="";
    $("#add_research_area > .research_area_list_entry > input").each(function(){
        if(this.checked == true){
            research_areas += this.id + "|";
        }
    });
    research_areas = research_areas.replace(/\|$/,"");
    
    abstract=$("#add_abstract").val();
    data={"p_id":<?php print $p_id; ?>, "title":title, "author_names":author_names, "author_users":author_users,"year":year,"publisher":publisher,"publisher_type":publisher_type, "research_area":research_areas,"abstract":abstract};
    if(force)
        data["force_insert"]="true";
    $.post("backend/?method=7", data).done(function(resp){
        if(resp["code"]==301){
            $("#signin_response").html('<div class="h_separator"></div>The title you entered conflicts with some other publication in the database. Are you sure you still want to insert?<div class="submit_button" onclick="add_paper(true);">Yes</div> <div class="submit_button" onclick="cancel_insert();">Cancel</div><br><div class="h_separator"></div>');
        }else if(resp["code"]!=200){
            $("#signin_response").html(resp["message"]);
        }else{
            window.location.href="pub.php?p_id=<?php print $p_id; ?>";
        }
    });
}
cancel_insert=function(){
    window.location.href="add_paper.php";
}
</script>

</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
		<?php require_once 'title.php'; ?>
		<div id="the_body">
            <div class="body_divs edit_box" style="width: 100%">
                <div class="list_header">
                    <div class="publications" >
                        Edit Paper Entry
                    </div>
                </div>
                <div class="h_separator"></div>
                <div class="edit_profile_details">
                    <div class="paper_box_entry">
                        Title: <input class="form_textbox" type="text" id="add_title" value="<?php print $title; ?>">
                        <div class="description"> Enter the title of the paper.</div>
                    </div>
                    
                    <div class="paper_box_entry">
                        Authors:                        
                        <table cellpadding="0" class="author_table">
                          <tbody id="author_body">  
            			    <tr>
                                <td>Name</td>
                                <td>&nbsp;ID</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><input class="table_edit_textbox author_name_box" type="text" value="<?php print $author_names[0]; ?>"></td>
                                <td>&nbsp;<input class="table_edit_textbox author_user_box" type="text" value="<?php print $author_users[0]; ?>"></td>
                                <td></td>
                            </tr>
                            <?php for($i=1;$i<$author_count;$i++){ ?>
                                <tr>
                                    <td><input class="table_edit_textbox author_name_box" type="text" value="<?php print $author_names[$i]; ?>"></td>
                                    <td>&nbsp;<input class="table_edit_textbox author_user_box" type="text" value="<?php print $author_users[$i]; ?>"></td>
                                    <td><div class="minus_sign" onclick="remove_author(this);">-</div></td>
                                </tr>
                            <?php } ?>

			              </tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><div class="plus_sign" onclick="add_author();">+</div></td>
                            </tr>
                        </table>
                        
                        <div class="description">
                            ID is optional, it is used to link profiles<br>
                            If author is a student his ID is his roll no.<br>
                            If he is a professor his ID is his email id without "@iith.ac.in" suffix. <br>
                            For eg: if the email id is "cse_prof@iith.ac.in" then ID is "cse_prof".
                        </div>
                    </div>
                    
                    <div class="paper_box_entry">
                        Date of Issue: <input class="edit_textbox" type="text" id="add_year" value="<?php print $year; ?>">
                        <div class="description"> Enter the year the paper was published.</div>
                    </div>
                    
                    <div class="paper_box_entry">
                        Conference/Journal Name: <input class="form_textbox" type="text" id="add_publisher" value="<?php print $publisher; ?>">
                        <div class="description"> Enter the name of the journal/conference the paper was published in.</div>
                    </div>
                    <div class="paper_box_entry">
                        Publication type: 
                        <select class = "select_menu" id="add_publisher_type">
                            <option value="journal" <?php if($publisher_type=="journal")print 'selected';?>>Journal</option>
                            <option value="series" <?php if($publisher_type=="series")print 'selected';?>>Conference</option>
                            <option value="booktitle" <?php if($publisher_type=="booktitle")print 'selected';?>>Book</option>
                        </select>
                    </div>
                    <div class="edit_research_areas">
                        Select Research Areas:
                        <div class="dropdown" style="margin-left:20px; font-size:16px;">
                            <div>
                                <div class="research_area_box">Research Areas
                                    <div class="arrow_down"></div>
                                </div>
                            </div>
                            <div class="research_area_checklist" id="add_research_area">
                                <?php 
                                    $result=mysql_query("select area_name, r_id from research_areas;");
                                    for($i=0;$i<mysql_num_rows($result);$i++){ 
                                        $row=mysql_fetch_row($result);
                                ?>
                                <div class="research_area_list_entry">
                                    <input type="checkbox" id="<?php print $row[1]; ?>"  <?php if(in_array($row[1],$selected_rids)){ print "checked=true";}?>   >
                                    <label for="<?php print $row[1]; ?>" class="box_label"><?php print $row[0]; ?></label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="description"> Choose a research area by selecting from the dropdown.</div>
                    </div>
                    <div class="edit_website">
                        <div style="float:left;">
                            Abstract :
                        </div>
                        <textarea class="website_box" cols="80" rows="10" id="add_abstract" style="max-width:80%;"><?php print $abstract; ?></textarea>
                        <div class="description">Paste the abstract of the paper in the above box.</div>
                    </div>
                    <div id="signin_response" style="text-align:left"></div>
                    <div>
                        <div class="submit_button" style="display:inline-block; width: 100px;" onclick="add_paper();">
    							Save
                        </div>
                        <div class="delete_button" style="display:inline-block; max-width:300px; width:150px" onclick="delete_paper();">
                                Delete Entry
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html> 
