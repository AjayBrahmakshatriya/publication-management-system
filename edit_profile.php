<?php
	require_once 'escape_get_post.php';
	session_start();
	if(!isset($_SESSION["master"]) || $_SESSION["master"]==""){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;	
	}
	$profile_id=$_SESSION["master"];
	require_once 'connect.php';
	$result = mysql_query("select name, websites, description from user_profiles where username='$profile_id';");
	$row=mysql_fetch_row($result);
	$name = $row[0];
	$websites=explode(',',$row[1]);
    $description = $row[2];
    $description = str_replace("<br />", "\n", $description);
?>



<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<title><?php print $name; ?></title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/paperList.css">
<link rel="stylesheet" type="text/css" href="styles/profile.css">
<link rel="stylesheet" type="text/css" href="styles/search_results.css">
<link rel="stylesheet" type="text/css" href="styles/edit_profile.css">
<link rel="stylesheet" type="text/css" href="styles/signIn.css">

<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>


<script>
update_info=function(){
	name=$("#edit_name").val().trim();
	if(name==""){
		$("#signin_response").html("Please fill in a name");
		return;
	}
	research_areas="";
	$("#edit_research > .research_area_list_entry > input:checked").each(function(index){
		research_areas+=$(this).attr("id")+"|";
	});
	research_areas=research_areas.replace(/\|$/, "");
	websites=$("#edit_websites").val().split("\n").join(",");
	desc=$("#edit_description_box").val().replace(/\n\r?/g, '<br />');
	desc = encodeURIComponent(desc);
	$("#signin_response").html("");
	$("#update_info").html("Updating...");	
	$.get("backend/?method=4&name="+name+"&research_areas="+research_areas+"&websites="+websites+"&description="+desc).done(function(resp){
		if(resp["code"]!=200){
			$("#signin_response").html(resp["message"]);
		}else{
            window.location.href="profile.php?id=<?php print $profile_id; ?>";
        }
		$("#update_info").html("Save");	
	});
}

image_upload = function(){
    $.ajax({"url" : "backend/?method=8", type: "POST", data : new FormData($("#upload_image_form")[0]),processData: false,
    contentType: false}).done(function(resp){
        if(resp["code"] == 200){
            document.getElementById('profile_picture').src = "images/profile_images/<?php print $profile_id; ?>.png?"+new Date().getTime();
        } else{
            $("#upload_response").html(resp["message"]);
        }
        $("#upload_image_button").html("Upload");
    });
    $("#upload_image_button").html("Uploading...");
}

</script>
</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
		<?php require_once 'title.php';?>
        <h1 class="research_area_title">
            <?php print $name; ?>
        </h1>
		<div id="the_body">
			<div class="body_divs rA_left_box" >
                <div class="h_separator" style="margin-bottom: .618em"></div>               
                <div style="overflow:auto;">
                    <img src="images/profile_images/<?php print $profile_id; ?>.png" height="155" class="profile_picture" id="profile_picture">
                </div>
                <form id="upload_image_form"><input class="choose_file" type="file" name="profile_image" accept="image/*"/></form>
                <div class="description"> 
                    Choose an image to set as your profile picture.
                </div>
                <div class="submit_button" onclick="image_upload();" id="upload_image_button">Upload</div>
                <div class="image_error_message" id="upload_response"></div>
            </div>
            <div class="body_divs" id="research_areas" >

                <div class="h_separator"></div>

                <div class="list_header">
                    <div class="publications">
                        Edit Profile <br>
                    </div>
                </div>

                <div class="h_separator"></div>
                <div class="edit_profile_details">

                    <div class="edit_name">
                        Name : <input id="edit_name" class="edit_textbox" type="text" name="FirstName" value="<?php print $name; ?>" style="width:50%">
                        <div class="description"> Enter your name as you wish it to be seen on your profile.</div>
                    </div>

                    <div class="edit_research_areas">
                        Select Research Areas:
                        <div class="dropdown" style="margin-left:20px;">
                            <div>
                                <div class="research_area_box">Research Areas
                                    <div class="arrow_down"></div>
                                </div>
                            </div>
                            <div class="research_area_checklist" id="edit_research">
				<?php 
					$research_result = mysql_query("SELECT A.r_id, 
                                                           area_name, 
                                                           COUNT(username) 
                                                    FROM   research_areas AS A 
                                                    LEFT OUTER JOIN user_research_area_mapping AS B 
                                                                 ON A.r_id = B.r_id 
                                                                 AND username = '$profile_id' 
                                                    GROUP  BY A.r_id; ");
					for ( $i=0;$i<mysql_num_rows($research_result); $i++){ 
						$row = mysql_fetch_row($research_result);
						?>
						<div class="research_area_list_entry">
							<input type="checkbox" id="<?php print $row[0]; ?>" <?php if($row[2]!="0") print 'checked'; ?> >
							<label for="<?php print $row[0]; ?>" class="box_label"><?php print $row[1]; ?></label>
						</div>
						<?php } ?>
                            </div>
                        </div>
                        <div class="description"> Choose multiple research areas by ticking on them.</div>
                    </div>

                    <div class="edit_website">
                        <div style="float:left;">
                            Websites :
                        </div>
                        <textarea class="website_box" cols="34" rows="3" id="edit_websites"><?php 
                            for($i=0;$i<count($websites);$i++)
                                print $websites[$i]."\n";
                        ?></textarea>
                        <div class="description">If there are multiple websites please enter each on a new line.</div>
                    </div>
                    
                    <div class="edit_description">
                        <div style="float:left;">
                            Description :
                        </div>
                        <textarea class="description_box" cols="40" rows="5" id="edit_description_box"><?php 
                                print $description;
                        ?></textarea>
                        <div class="description">Enter a description of your research work.</div>
                    </div>
                    
                    <div class="submit_button" id="update_info" onclick="update_info();">
							Save
                    </div>
    		    <div id="signin_response" style="text-align:left"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html> 
