<?php
  require_once 'connect.php';
  require_once 'escape_get_post.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<title>Publications@cse.iith</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
</head>

<body style="max-width:978px; margin:auto; ">
  <div id='outline'>
  <?php require_once 'title.php';?>
    <br>
    <div id="the_body">
      <div class="body_divs main_left_box">
      <!-- CSE department, IITH likes to do research... Placeholder text for department research motto? -->

    </div>
      <div class="body_divs" id="research_areas" >
        <div style="margin-top:30px; font-size:30px; text-align: left; margin-left: 10px">
          Research Areas
        </div>
        <div id="research_areas_container">
          <?php
            $result = mysql_query("select A.r_id, area_name, count(p_id) from research_areas as A left outer join publication_research_area_mapping as B on A.r_id = B.r_id group by A.r_id ;");
            $count = mysql_num_rows($result);
            for ($i=0;$i<$count;$i++){
              $row = mysql_fetch_row($result);
            ?>
              <div class="h_separator"></div>
              <div class="research_area">
                <div class="research_area_text">
                  <a href="research_area.php?r_id=<?php print $row[0];?>"><?php print $row[1];?></a>
                </div>
                <a href="research_area.php?r_id=<?php print $row[0];?>">
                  <div class="research_area_button">
                    <?php print $row[2]; print " publication"; if($row[2]!=1) print "s";?>
                  </div>
                </a>
              </div>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <?php require_once 'footer.php';?>

  <div id="wrap">
    <div id="main">
      <hr>
      <p>
        <ul id="navigationFooter">
          <li>
            <a href="siteContributors.php">Site Contributors</a>
          </li>
        </ul>
      </p>
    </div>
  </div>

</body>
</html>
