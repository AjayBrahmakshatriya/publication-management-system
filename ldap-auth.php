<?php
require_once 'connect.php';

function close_with_failure($ldap)
{
	$response = array();
	@ldap_close($ldap);
	$response["success"] = false;
	return $response;
}

function ldap_auth_iith($hostname, $uname, $password)
{
	$adServer = "ldap://" . $hostname;
	$ldap = ldap_connect($adServer);

	$filter = "(uid=" . $uname . ")";
	$result = ldap_search($ldap,"ou=people,dc=iith,dc=ac,dc=in",$filter);
	$info = ldap_get_entries($ldap, $result);

	$response = array();
	/* user not there in ldap directory */
	if($info["count"] == 0){
		return close_with_failure($ldap);
	}
	$ldapDn = $info[0]["dn"];
	/* extract role */
	$arr = explode(",", $ldapDn);
	$role = "none";
	$name = "xyz";
	foreach ($arr as $i){
		$temp = explode("=",$i);
		if ($temp[1] == "student"){
			$role = "student";
			$name = $info[0]["uid"][0];
			break;
		} else if ($temp[1] == "faculty"){
			$role = "faculty";
			$name = $info[0]["cn"][0];
			break;
		}
	}

	if ($role == "faculty"){
		$result = mysql_query("SELECT uid FROM faculty_list WHERE uid='" . $info[0]["uid"][0] . "';");

		/* Check if faculty is from another department.*/
		if (mysql_num_rows($result) == 0){
			return close_with_failure($ldap);
		}
	} else if ($role == "student"){
		/* Check if student is from another department.*/
		$dept = substr($name,0,2);
		if (!(strcasecmp($dept,"cs") == 0)){
			return close_with_failure($ldap);
		}
	} else {
		return close_with_failure($ldap);
	}
	/* Authentication */

	if ($role == "faculty"){
		$result = mysql_query("SELECT password FROM faculty_auth WHERE uname='" . $info[0]["uid"][0] . "';");
		$row = mysql_fetch_row($result);
		$pass = $row[0];
		if (md5($password) == $pass){
			@ldap_close($ldap);
				$response["success"] = true;
        $response["role"] = $role;
        $response["name"] = $name;
	      return $response;
		}
	}

	$bind = @ldap_bind($ldap, $ldapDn, $password);
	if($bind) {
		@ldap_close($ldap);
		$response["success"] = true;
		$response["role"] = $role;
		$response["name"] = $name;
		return $response;
	}
	else {
		return close_with_failure($ldap);
	}
}
?> 
