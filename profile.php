<?php
	require_once 'escape_get_post.php';
	if(!isset($_GET["id"]) || $_GET["id"] == ""){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;
	}
	$id = $_GET["id"];
	require_once 'connect.php';
	$result=mysql_query("SELECT NAME,
                                   websites,
                                   description
                            FROM   user_profiles
                            WHERE  username = '$id'; ");
	if (mysql_num_rows($result)==0){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;
	}
	$row = mysql_fetch_row($result);
	$name = $row[0];
	$website = $row[1];
    $description = $row[2];
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<title><?php print $name;?> </title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/paperList.css">
<link rel="stylesheet" type="text/css" href="styles/profile.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>

<script>

desc_state = 0;
toggle_show=function(){
    if(desc_state == 0){
        desc_state = 1;
        $(".rest_of_content").show(0);
        $(".rest_of_content").css({"display":"inline"});
        $("#show_button").html("Show less");
        intro_with_dots = document.getElementsByClassName("intro_content")[0].innerHTML;
        intro = intro_with_dots.substring(0,intro_with_dots.length - 3);
        document.getElementsByClassName("intro_content")[0].innerHTML = intro;
    } else{
        intro = document.getElementsByClassName("intro_content")[0].innerHTML;
        document.getElementsByClassName("intro_content")[0].innerHTML = intro + "...";
        desc_state = 0;
        $(".rest_of_content").hide(0);
        $("#show_button").html("Show more");
    }
}
sort_asc=function(){
        $("#research_areas_container_reverse").hide(0);
        $("#research_areas_container").show(0);
        $("#asc").css({"text-decoration":"none"});
        $("#desc").css({"text-decoration":"underline"});
}
sort_desc=function(){
        $("#research_areas_container").hide(0);
        $("#research_areas_container_reverse").show(0);
        $("#desc").css({"text-decoration":"none"});
        $("#asc").css({"text-decoration":"underline"});
}
show_all_pubs=function(){
    $(".not_my_authored").show(0);
    $("#all_pub").css({"text-decoration":"none"});
    $("#my_authored").css({"text-decoration":"underline"});
}
show_my_authored=function(){
    $(".not_my_authored").hide(0);
    $("#my_authored").css({"text-decoration":"none"});
    $("#all_pub").css({"text-decoration":"underline"});
}
</script>
</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
	<?php require_once 'title.php';?>
		<br>
        <h1 class="research_area_title">
            <?php print $name; ?>
        </h1>
		<div id="the_body">
			<div class="body_divs rA_left_box" >
                <div class="h_separator" style="margin-bottom: .618em"></div>               
                <div style="overflow:auto;">
                    <img src="images/profile_images/<?php print $id; ?>.png" height="155" class="profile_picture">
                    <br>
                    Research Area(s):
                   
                    <div>
			<?php
				$result = mysql_query(" SELECT A.r_id,
                                               area_name
                                        FROM   research_areas AS A,
                                               user_research_area_mapping AS B
                                        WHERE  A.r_id = B.r_id
                                               AND B.username = '$id'; ");
				if(mysql_num_rows($result)==0){
					print 'No research areas associated with this user';
				}else{
					$count = mysql_num_rows($result);
					for($i=0;$i<$count;$i++){
						$row = mysql_fetch_row($result);
						?>
						<a href="research_area.php?r_id=<?php print $row[0];?>" class="profile_research_area"><?php print $row[1];?></a><br>
						<?php
					}
				}?>

                    </div>

			<?php $websites = explode(',', $website);
				$count = count($websites);
                if($websites[0]){
                    print '<br>Website:<br>';
                }
				for($i=0;$i<$count;$i++){
					$clean_website=trim($websites[$i]);	
				?>		
		                        <a class="profile_website" href="http://<?php print $clean_website;?>"><?php print $clean_website;?></a></br>
				<?php } ?>
                </div>
            </div>
            <div class="body_divs" id="research_areas" >
                <!--Start of description block-->
                <?php
                    if(strlen($description) != 0){
                ?>
                    <div class="h_separator"></div>

                    <div class="about_me">
                            <?php
                                $desc = str_replace("<br />","\n",$description);
                                $preview_len = 460;
                                if (strlen($desc) > $preview_len){
                                    $first = substr($desc,0,$preview_len);
                                    $second = substr($desc,$preview_len);
                                    $first = str_replace("\n", "<br/>", $first);
                                    $second = str_replace("\n", "<br/>", $second);
                                    print '<span class=intro_content>' . $first . "...</span>";
                            ?><span class="rest_of_content"><?php
                                    print $second;
                            ?>
                            </span>
                            <br>
                            <button id="show_button" onclick="toggle_show()">Show more</button>
                            <?php
                                } else {
                                    print $desc;
                                }
                            ?>
                    </div>
                <?php } ?>
                <!--End of description block-->

                <div class="h_separator"></div>

                <div class="list_header">
                    <div class="publications" >
                        Publications <br>
                        <?php if($logged_in){ ?>
                        <div class="whose_publication_filter">
                            <a id="all_pub" href="#" onclick="show_all_pubs();">All</a> |
                            <a id="my_authored" href="#" onclick="show_my_authored();" style="text-decoration: underline;">My Authored</a>                        
                        </div>
                        <?php } ?>
                    </div>

                    <div class="sortby" <?php if($logged_in) print 'style="margin-top:10px;"'; ?>> <!--put margin-top only if the user is signed in-->
                        Sort by DOI<br>
                        <a id="asc" href="#" onclick="sort_asc();">Ascending</a> |
                        <a id="desc" href="#" onclick="sort_desc();" style="text-decoration:underline">Descending</a>
                    </div>
                </div>

            <div id="research_areas_container" style="margin-top: 10px">
    			<?php 
                if ($logged_in && $id == $username )
                    $result=mysql_query("   SELECT A.p_id,
                                                   title,
                                                   publisher,
                                                   year,
                                                   owner,
                                                   Group_concat(name ORDER BY author_order SEPARATOR '|'),
                                                   Group_concat(username ORDER BY author_order SEPARATOR '|'),
                                                   abstract
                                            FROM   publications AS A
                                                   JOIN authors AS B
                                                     ON A.p_id = B.p_id
                                            WHERE  A.p_id IN (SELECT p_id
                                                              FROM   authors
                                                              WHERE  username = '$id'
                                                              UNION ALL
                                                              SELECT p_id
                                                              FROM   publications
                                                              WHERE  owner = '$username')
                                            GROUP  BY A.p_id
                                            ORDER  BY year; ");
    			else
                    $result=mysql_query("   SELECT A.p_id,
                                                   title,
                                                   publisher,
                                                   year,
                                                   owner,
                                                   Group_concat(name ORDER BY author_order SEPARATOR '|'),
                                                   Group_concat(username ORDER BY author_order SEPARATOR '|'),
                                                   abstract
                                            FROM   publications AS A
                                                   JOIN authors AS B
                                                     ON A.p_id = B.p_id
                                            WHERE  A.p_id IN (SELECT p_id
                                                              FROM   authors
                                                              WHERE  username = '$id')
                                            GROUP  BY A.p_id
                                            ORDER  BY year; ");

                $count = mysql_num_rows($result);
    			for($i=0;$i<$count;$i++){
    			$row=mysql_fetch_row($result);
    			$author_names = explode('|',$row[5]);
                $author_users = explode('|',$row[6]);
                $author_count = count($author_names);
                $owner = $row[4];
                $my_authored=$logged_in && in_array($username, $author_users);
                ?>
                
                 <?php if (!$my_authored){?>
                    <div class="h_separator not_my_authored"></div>
                    <div class="research_area not_my_authored">
                    <?php } else { ?>
                    <div class="h_separator"></div>
                    <div class="research_area">
                <?php } ?>

                    <?php if (!$logged_in || $username != $owner){?>
                        <div class="paper_title">
                            <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                        </div>
                    <?php } else { ?>
                        <div style="overflow:auto; margin-right:10px" >                            
                            <div class="paper_title" style="float:left; max-width: 500px;">
                                <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                            </div>
                            <div class="edit_entry">
                                <a href="edit_paper.php?p_id=<?php print $row[0]; ?>">Edit Entry</a>
                            </div>                    
                        </div>
                    <?php } ?>
                    <div class="author_names">
                    <?php for($j=0;$j<$author_count;$j++){ ?>
                            <?php if ($author_users[$j]!="") {?>
                            <a href="profile.php?id=<?php print $author_users[$j]; ?>" class="author_name"><?php print $author_names[$j]; ?></a>
                            <?php } else { print $author_names[$j]; } ?>
                            <?php if ($j!=$author_count-1)print '|'; ?>
                    <?php } ?>
                    </div>
                    <div class="conference_journal_name">
                            <?php print $row[2]; ?> (<?php print $row[3]; ?>)
                    </div>
                </div>
                <?php } ?>
            </div>
            <div id="research_areas_container_reverse" style="margin-top: 10px;display:none">            	
    			<?php 
                $result=mysql_query("   SELECT A.p_id,
                                               title,
                                               publisher,
                                               year,
                                               owner,
                                               Group_concat(name ORDER BY author_order SEPARATOR '|'),
                                               Group_concat(username ORDER BY author_order SEPARATOR '|'),
                                               abstract
                                        FROM   publications AS A
                                               JOIN authors AS B
                                                 ON A.p_id = B.p_id
                                        WHERE  A.p_id IN (SELECT p_id
                                                          FROM   authors
                                                          WHERE  username = '$id')
                                        GROUP  BY A.p_id
                                        ORDER  BY year DESC; ");
    			$count = mysql_num_rows($result);
    			for($i=0;$i<$count;$i++){
    			$row=mysql_fetch_row($result);
    			$author_names = explode('|',$row[5]);
                $author_users = explode('|',$row[6]);
                $author_count = count($author_names);
                $owner = $row[4];
                ?>
                <div class="h_separator"></div>
                <div class="research_area">
                    <?php if (!$logged_in || $username != $owner){?>
                        <div class="paper_title">
                            <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                        </div>
                    <?php } else { ?>
                        <div style="overflow:auto; margin-right:10px" >                            
                            <div class="paper_title" style="float:left; max-width: 500px;">
                                <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                            </div>
                            <div class="edit_entry">
                                <a href="edit_paper.php?p_id=<?php print $row[0]; ?>">Edit Entry</a>
                            </div>                    
                        </div>
                    <?php } ?>
                    <div class="author_names">
                        <?php for($j=0;$j<$author_count;$j++){ ?>
                        <?php if ($author_users[$j]!="") {?>
                        <a href="profile.php?id=<?php print $author_users[$j]; ?>" class="author_name"><?php print $author_names[$j]; ?></a>
                        <?php } else { print $author_names[$j]; } ?>
                        <?php if ($j!=$author_count-1)print '|'; ?>
                        <?php } ?>
                    </div>
                    <div class="conference_journal_name">
                        <?php print $row[2]; ?> (<?php print $row[3]; ?>)
                    </div>
                </div>
                <?php } ?>
    		</div>
        </div>
        </div>
    </div>
</body>
</html> 
