<?php
	require_once 'escape_get_post.php';
	if(!isset($_GET["p_id"]) || $_GET["p_id"]==""){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;
	}
	$p_id = $_GET["p_id"];
	require_once 'connect.php';
	$result=mysql_query("select title, publisher, year, owner, GROUP_CONCAT(name order by author_order separator '|'), GROUP_CONCAT(username order by author_order separator '|'), abstract, publisher_type from publications as A join authors as B on A.p_id = B.p_id where A.p_id='$p_id' group by A.p_id;");
	if(mysql_num_rows($result)==0){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;	
	}
	$row = mysql_fetch_row($result);
	$title = $row[0];
	$publisher = $row[1];
	$year = $row[2];
	$owner = $row[3];
	$authors = $row[4];
	$author_names = explode('|',$row[4]);
	$author_count = count($author_names);
	$author_users = explode('|',$row[5]);
	$abstract = $row[6];
    $publisher_type = $row[7];
?>



<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<title><?php print $title; ?></title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/pubs.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
		<?php require_once 'title.php';?>
		<br>
        <h1 class="research_area_title">
            <?php print $title; ?>
        </h1>
		<div id="the_body">
			<div class="body_divs pubs_info" >
                <div class="h_separator" style="margin-bottom: .618em"></div>                
                <div class="mini_title">Conference/Journal</div>
                <div class="mini_title_info"><?php print $publisher.'('.$year.')'; ?></div>
                
                <div class="mini_title">Publication Year</div>
                <div class="mini_title_info"><?php print $year; ?></div>
                
                <div class="mini_title">Authors</div>
                <div class="author_names">
                	<?php for($j=0;$j<$author_count;$j++){ ?>
                                        <?php if ($author_users[$j]!="") {?>
                                        <a href="profile.php?id=<?php print $author_users[$j]; ?>" class="author_name"><?php print $author_names[$j]; ?></a>
                                        <?php } else { print $author_names[$j]; } ?>
                                        <?php if ($j!=$author_count-1)print '|'; ?>
                        <?php } ?>
    
                </div>
                
                <div class="mini_title">BibTex</div>
		<textarea class="bibtex_box" cols="34" rows="15"><?php 
            $result=mysql_query("select attr, value from bibtex_entries where p_id='$p_id';");
            if (mysql_num_rows($result)==0){
                $disp_id=array("journal"=>"article", "series"=>"conference", "booktitle"=>"book");
                $disp_id=$disp_id[$publisher_type];
                $temp = preg_replace('/[^A-Za-z0-9\-]/', '', $author_names[0]).$year.explode(' ',$title);
                $unique_id=$temp[0];
                print "@$disp_id{{$unique_id},\ntitle={".$title."},\nauthor={".implode(' and ',$author_names)."},\nyear=$year\n}"; 
            }else{
                $fields=array();
                for($i=0;$i<mysql_num_rows($result);$i++){
                    $row=mysql_fetch_row($result);
                    $fields[$row[0]]=$row[1];
                }
                $disp_id=$fields["x-bibtex-type"];
                $unique_id=$fields["key"];
                unset($fields["x-bibtex-type"]);
                unset($fields["key"]);
                print "@$disp_id{{$unique_id},\ntitle={".$title."},\nauthor={".implode(' and ',$author_names)."},\nyear=$year";
                foreach ($fields as $key=>$value){
                    print ",\n$key={{$value}}";
                }
                print "\n}";
            }
            ?></textarea>                
        
            </div>

            <div class="body_divs" id="research_areas" >

                <div class="h_separator"></div>

                <?php if(!$logged_in || $username != $owner) {?>
                <div class="abstract_title" >
                    Abstract 
                </div>
                <?php } else { ?>
                <div>
                    <div class="abstract_title" style="display:inline-block;">
                        Abstract 
                    </div>

                    <div class="pub_edit_entry">
                        <a href="edit_paper.php?p_id=<?php print $p_id; ?>">Edit Entry</a>
                    </div>
                </div>    
                <?php } ?>

                <div class="h_separator"></div>
                
                <div class="abstract_text" id="abstract">
            		<?php print str_replace("\n","<br>",$abstract); ?>
                </div>

            </div>
        </div>
    </div>
</body>
</html> 
