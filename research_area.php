<?php
	require_once 'escape_get_post.php';
	require_once 'connect.php';
	if (!isset($_GET["r_id"]) || $_GET["r_id"]==""){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;
	}	
	$r_id = $_GET["r_id"];
	$result=mysql_query("select area_name, description from research_areas where r_id='$r_id';");
	if (mysql_num_rows($result)==0){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;	
	}
	$row = mysql_fetch_row($result);
	$area_name = $row[0];
	$area_description = $row[1];

    $result = mysql_query(" SELECT   a.p_id, 
                                     title, 
                                     owner, 
                                     year, 
                                     publisher, 
                                     group_concat(NAME order BY author_order separator '|'), 
                                     group_concat(username ORDER BY author_order separator '|') 
                            FROM     publications                      AS a 
                            JOIN     authors                           AS b 
                            JOIN     publication_research_area_mapping AS c 
                            ON       a.p_id = b.p_id 
                            AND      a.p_id = c.p_id 
                            WHERE    c.r_id='$r_id' 
                            GROUP BY a.p_id 
                            ORDER BY year;");
	$count = mysql_num_rows($result);
	
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<title><?php print $area_name; ?></title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/paperList.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
<script>
sort_asc=function(){
        $("#research_areas_container_reverse").hide(0);
        $("#research_areas_container").show(0);
        $("#asc").css({"text-decoration":"none"});
        $("#desc").css({"text-decoration":"underline"});
}
sort_desc=function(){
        $("#research_areas_container").hide(0);
        $("#research_areas_container_reverse").show(0);
        $("#desc").css({"text-decoration":"none"});
        $("#asc").css({"text-decoration":"underline"});
}
show_all_pubs=function(){
    $(".not_my_pubs").show(0);
    $("#all_pub").css({"text-decoration":"none"});
    $("#my_pub").css({"text-decoration":"underline"});
}
show_my_pubs=function(){
    $(".not_my_pubs").hide(0); 
    $("#my_pub").css({"text-decoration":"none"});
    $("#all_pub").css({"text-decoration":"underline"});  
}
</script>
</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
        <?php require_once 'title.php';?>
		<br>
        <h1 class="research_area_title">
            <?php print $area_name; ?>
        </h1>
		<div id="the_body">
			<div class="body_divs rA_left_box" style="word-wrap:break-word">
                <div class="h_separator" style="margin-bottom: .618em"></div>                
            	<?php print $area_description; ?>
            </div>

            <div class="body_divs" id="research_areas" >
                <div class="h_separator"></div>

                <div class="list_header">
                    <div class="publications" >
                        <?php print $count; ?> Publications <br>
            			<?php if($logged_in){ ?>
                        <div class="whose_publication_filter">
                            <a id="all_pub" href="#" onclick="show_all_pubs();">All</a> |
                            <a id="my_pub" href="#" onclick="show_my_pubs();" style="text-decoration: underline;">My Publications</a>                        
                        </div>
            			<?php } ?>
                    </div>
                    <div class="sortby" <?php if($logged_in) print 'style="margin-top:10px;"'; ?>> <!--put margin-top only if the user is signed in-->
                        Sort by DOI<br>
                        <a id="asc" href="#" onclick="sort_asc();">Ascending</a> |
                        <a id="desc" href="#" onclick="sort_desc();" style="text-decoration:underline">Descending</a>
                    </div>
                </div>

                <div id="research_areas_container" style="margin-top: 10px">
        		    <?php 
        			for ($i=0;$i<$count;$i++){
        			$row = mysql_fetch_row($result);
        			$author_names = explode('|',$row[5]);
        			$author_users = explode('|',$row[6]);
        			$author_count = count($author_names);
                    $owner = $row[2];

                    $my_pub = $logged_in && in_array($username, $author_users);
        		    ?>

                    <?php if (!$my_pub){?>
        			<div class="h_separator not_my_pubs"></div>
        			<div class="research_area not_my_pubs">
                    <?php } else { ?>
                    <div class="h_separator"></div>
                    <div class="research_area">
                    <?php } ?>

                        <?php if (!$logged_in || $username != $owner){?>
        				<div class="paper_title">
        					<a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
        				</div>
                        <?php } else { ?>
                        <div style="overflow:auto; margin-right:10px" >                            
                            <div class="paper_title" style="float:left; max-width: 500px;">
                                <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                            </div>
                            <div class="edit_entry">
                                <a href="edit_paper.php?p_id=<?php print $row[0]; ?>">Edit Entry</a>
                            </div>                    
                        </div>
                        <?php } ?>
        				<div class="author_names">
        				    <?php for($j=0;$j<$author_count;$j++){ ?>
        					<?php if ($author_users[$j]!="") {?>
        					<a href="profile.php?id=<?php print $author_users[$j]; ?>" class="author_name"><?php print $author_names[$j]; ?></a>
        					<?php } else { print $author_names[$j]; } ?>
        					<?php if ($j!=$author_count-1)print '|'; ?> 
        				    <?php } ?>
        				</div>
        				<div class="conference_journal_name">
        					<?php print $row[4]; ?> (<?php print $row[3]; ?>)
        				</div>
        			</div>
                    <?php } ?>

                </div>
                <div id="research_areas_container_reverse" style="margin-top: 10px; display:none;">
        		    <?php 
        			$result = mysql_query(" SELECT   a.p_id, 
                                     title, 
                                     owner, 
                                     year, 
                                     publisher, 
                                     group_concat(NAME order BY author_order separator '|'), 
                                     group_concat(username ORDER BY author_order separator '|') 
                            FROM     publications                      AS a 
                            JOIN     authors                           AS b 
                            JOIN     publication_research_area_mapping AS c 
                            ON       a.p_id = b.p_id 
                            AND      a.p_id = c.p_id 
                            WHERE    c.r_id='$r_id' 
                            GROUP BY a.p_id 
                            ORDER BY year DESC;");
        			$count = mysql_num_rows($result);
        			for ($i=0;$i<$count;$i++){
        			$row = mysql_fetch_row($result);
        			$author_names = explode('|',$row[5]);
        			$author_users = explode('|',$row[6]);
        			$author_count = count($author_names);
        			$owner = $row[2];  
                    $my_pub = $logged_in && in_array($username, $author_users);                  
        		    ?>

        			<?php if (!$my_pub){?>
                    <div class="h_separator not_my_pubs"></div>
                    <div class="research_area not_my_pubs">
                    <?php } else { ?>
                    <div class="h_separator"></div>
                    <div class="research_area">
                    <?php } ?>
            			<?php if (!$logged_in || $username != $owner){?>
                        <div class="paper_title">
                            <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                        </div>
                        <?php } else { ?>
                        <div style="overflow:auto; margin-right:10px" >                            
                            <div class="paper_title" style="float:left; max-width: 500px;">
                                <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                            </div>
                            <div class="edit_entry">
                                <a href="edit_paper.php?p_id=<?php print $row[0]; ?>">Edit Entry</a>
                            </div>                    
                        </div>
                        <?php } ?>
        				<div class="author_names">
            				<?php for($j=0;$j<$author_count;$j++){ ?>
        					<?php if ($author_users[$j]!="") {?>
        					<a href="profile.php?id=<?php print $author_users[$j]; ?>" class="author_name"><?php print $author_names[$j]; ?></a>
        					<?php } else { print $author_names[$j]; } ?>
        					<?php if ($j!=$author_count-1)print '|'; ?> 
            				<?php } ?>
        				</div>
        				<div class="conference_journal_name">
        					<?php print $row[4]; ?> (<?php print $row[3]; ?>)
        				</div>
        			</div>   <!-- One publication entry box (research_area) -->
        			<?php } ?>
                </div> <!-- Reverse container box -->
            </div> <!-- Research areas right box-->
        </div> <!-- The body -->
    </div>
    <?php require_once 'footer.php';?>

    <div id="wrap">
        <div id="main">
            <hr>
            <p>
                <ul id="navigationFooter">
                    <li>
                        <a href="siteContributors.php">Site Contributors</a>
                    </li>
                </ul>
            </p>
        </div>
    </div>

</body>
</html> 
