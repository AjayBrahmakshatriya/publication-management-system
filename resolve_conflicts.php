<?php
	require_once 'escape_get_post.php';
	session_start();
	if (!isset($_SESSION["master"]) || $_SESSION["master"]=="" || $_SESSION["role"]!="faculty"){
		print '<html><head><script>window.location.href=".";</script></head></html>';
                return;
	}
	
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<title>Resolve Conflicts</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/paperList.css">
<link rel="stylesheet" type="text/css" href="styles/resolve_conflicts.css">
<link rel="stylesheet" type="text/css" href="styles/edit_profile.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
<script>
submit_entries=function(){
    choice_string="";
    $(".accept_conflict > input:checked").each(function(index){
        choice_string+=$(this).val()+"|";
    });
    choice_string = choice_string.replace(/\|$/, "");
    $.post("backend/?method=9", {"choice_string":choice_string}).done(function(resp){
        $("#submit_conflicts").val("Sumbit");
        if(resp["code"]==200){
            window.location.href=window.location.href;
        }
    });
    $("#submit_conflicts").val("Sumbitting...");
}
</script>
</head>    
<body style="max-width:978px; margin:auto; ">
<div id='outline'>
    <?php require_once 'title.php';?>
    <h1 class="research_area_title">
        Resolve conflicts        
    </h1>
    <div class="describing_box">
        This conflict occurred due to duplicate titles. You can choose to insert new entries without removing exsisting entries.
    </div>
    <div id="the_body">
        <div class="body_divs list_of_conflicts">
            <div class="h_separator"></div>
            <!--Repeat from here-->
            <?php for($i=0;$i<count($_SESSION["conflict_entries"]);$i++){ $entry = $_SESSION["conflict_entries"][$i];?>
	    <div class="research_area">
                <div style="overflow:auto; margin-right:10px" >                            
                    <div class="conflicting_paper_title" style="float:left; max-width: 500px;">
                        <?php print $entry["title"]; ?>
                    </div>
                    <div class="accept_conflict">
                        Force Insert
                        <input type="radio" name="<?php print $i; ?>" value="true"> Yes
                        <input type="radio" name="<?php print $i; ?>" value="false" checked> No
                    </div>
                </div>
                <div class="conflicting_box">
                    Conflicting Paper : 
                    <a class="conflicting_link" href="pub.php?p_id=<?php print $entry["_conflict_id"];?>">pub.php?p_id=<?php print $entry["_conflict_id"];?></a>
                </div>
            </div>
		<?php } 
        if(count($_SESSION["conflict_entries"])==0){
            print '<br><center>There are no more conflicts, you can return to home page</center>';
        }else{
            ?>
            <div id="submit_conflicts" onclick="submit_entries();" class="submit_button" style="margin-left:10px;">Submit</div>
            <?php
        }
        ?>
            <!--Repeat till here-->

        </div>
    </div>
</div>
</body>
</html>
