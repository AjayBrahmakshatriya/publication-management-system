<?php
	require_once 'escape_get_post.php';
	if(!isset($_GET["search_query"]) || trim($_GET["search_query"])==""){
		print '<html><head><script>window.location.href=".";</script></head></html>';
                return;
	}
	$search_string = trim(strtolower($_GET["search_query"]));
	$broken=explode(" ", $search_string);
        $query="SELECT p_id 
                FROM   (SELECT P.p_id                           AS p_id, 
                               title, 
                               abstract, 
                               Group_concat(name SEPARATOR ' ') AS pauthors 
                        FROM   publications AS P 
                               JOIN authors AS Q 
                                 ON P.p_id = Q.p_id 
                        GROUP  BY P.p_id) AS R 
                WHERE  1 = 0 ";
        for ($i=0;$i<count($broken);$i++){
                if (trim($broken[$i])!=""){
                        $query.="or title like '%{$broken[$i]}%' or abstract like '%{$broken[$i]}%' or pauthors like '%{$broken[$i]}%' ";
                }
        }
	require_once 'connect.php';

	$result = mysql_query(" SELECT   a.p_id, 
                                     title, 
                                     publisher, 
                                     year, 
                                     owner, 
                                     group_concat(NAME order BY author_order separator '|'), 
                                     group_concat(username ORDER BY author_order separator '|'), 
                                     abstract, 
                                     c.r_id 
                            FROM     publications                     AS a 
                            JOIN     authors                          AS b 
                            JOIN     publication_research_area_mapping AS c 
                            ON       a.p_id = b.p_id 
                            AND      a.p_id = c.p_id 
                            WHERE    a.p_id IN ( ".$query." ) 
                            GROUP BY a.p_id 
                            ORDER BY year; ");
    
	$count = mysql_num_rows($result);
	$result_research_areas=mysql_query("SELECT DISTINCT area_name, 
                                                        A.r_id 
                                        FROM   research_areas AS A 
                                        JOIN   publication_research_area_mapping AS B 
                                        ON     A.r_id = B.r_id 
                                        WHERE  B.p_id IN ( ".$query." );");
    
	$result_year=mysql_query("  SELECT MIN(year), 
                                       MAX(year) 
                                FROM   publications 
                                WHERE  p_id IN ( ".$query." ); ");
	$row=mysql_fetch_row($result_year);
	$min_year = intval($row[0]);
	$max_year = intval($row[1]);
	$result_authors = mysql_query(" SELECT DISTINCT NAME 
                                    FROM   authors 
                                    WHERE  p_id IN ( ".$query." )
                                    ORDER BY NAME;");
	
	if(isset($_GET["area_filter"])){
		$area_filter=true;
		$area_list=explode('|',$_GET["area_filter"]);
	}else{
		$area_filter=false;
	}
	if(isset($_GET["min_year"]) && $_GET["min_year"]!=""){
		$min_year_filter = intval($_GET["min_year"]);
		$min_flag = true;
	}else{
		$min_flag = false;
	}
	if(isset($_GET["max_year"]) && $_GET["max_year"]!=""){
		$max_year_filter = intval($_GET["max_year"]);
		$max_flag = true;
	}else{
		$max_flag = false;
	}
	if(isset($_GET["author_filter"])){
		$author_filter=true;
		$author_list=explode('|',$_GET["author_filter"]);
	}else{
		$author_filter=false;
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<title>Search</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/paperList.css">
<link rel="stylesheet" type="text/css" href="styles/search_results.css">
<link rel="stylesheet" type="text/css" href="styles/edit_profile.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
<script>
apply_filter=function(){
	search_query="<?php print $search_string; ?>";
	area_filter="";
	$("#research_area_filter > .research_area_list_entry > input:checked").each(function(index){
		area_filter+=$(this).attr("id")+"|";
	});	
	area_filter = area_filter.replace(/\|$/, "");
	author_filter="";
	$("#author_filter > .research_area_list_entry > input:checked").each(function(index){
		author_filter+=$(this).attr("id")+"|";
	});	
	author_filter = author_filter.replace(/\|$/, "");
	min_year=$("#min_year").val();
	max_year=$("#max_year").val();
	final_url="search_results.php?search_query="+search_query;
	final_url+="&area_filter="+area_filter;
	final_url+="&author_filter="+author_filter;
	if (min_year!="-1")
		final_url+="&min_year="+min_year;
	if (max_year!="-1")
		final_url+="&max_year="+max_year;
	window.location.href=(final_url);
}

tick_authors=function(){
    $("#author_filter > .research_area_list_entry > input:not(:checked)").each(function(){
        this.checked = true;
    });
}
                                                                               
untick_authors=function(){
    $("#author_filter > .research_area_list_entry > input:checked").each(function(index){
        $(this).attr('checked',false);
    });
}

tick_ra=function(){
    $("#research_area_filter > .research_area_list_entry > input:not(:checked)").each(function(index){
        this.checked = true;
    });
}
                                                                               
untick_ra=function(){
    $("#research_area_filter > .research_area_list_entry > input:checked").each(function(index){
        $(this).attr('checked',false);
    });
}
</script>

</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
		<?php require_once 'title.php';?>
        
		<div id="the_body">
            <div class="body_divs results_list">

                <div class="filters">
                    Filters:
                    
                    <div class="dropdown">
                        <div>
                            <div class="research_area_box">Research Areas
                                <div class="arrow_down"></div>
                            </div>
                        </div>
                        <div class="research_area_checklist" id="research_area_filter">
                            <div class="research_area_list_entry tick">
                                <div class="untick_all">
                                    <a class="research_tick_box" style="cursor:pointer;text-decoration:underline;" onclick="untick_ra()">Untick All</a>
                                </div>
                                <div class="tick_all">
                                    <a class="research_tick_box" style="cursor:pointer;text-decoration:underline;" onclick="tick_ra()">Tick All</a>
                                </div>
                            </div>
			    <?php
				for($i=0;$i<mysql_num_rows($result_research_areas);$i++){
					$row=mysql_fetch_row($result_research_areas);
			    ?>
				<div class="research_area_list_entry">
					<input type="checkbox" class="research_tick_box" id="<?php print $row[1]; ?>" <?php if ($area_filter==false || in_array($row[1], $area_list))print 'checked'; ?>>
                                	<label for="<?php print $row[1];?>" class="box_label"><?php print $row[0]; ?></label>
				</div>

			    <?php } ?>

                        </div>
                    </div>
                    
                    <!--<input type="checkbox" class="flag_checkbox" id="date_flag">-->
                    <div style="display:inline-block;margin-left:30px;">Date:</div>
                    <select class="date_range" id="min_year">
                        <option class="date_entry" value="-1" >From</option>
			<?php for($i=$min_year;$i<=$max_year && $min_year!=0 ;$i++){
				?>
                        		<option class="date_entry" value="<?php print $i; ?>" <?php if($min_flag && $min_year_filter==$i) print 'selected'; ?> ><?php print $i; ?></option>
				<?php
			} ?>
                    </select>
                    -
                    <select class="date_range" id="max_year">
                        <option class="date_entry" value="-1">To</option>
			<?php for($i=$min_year;$i<=$max_year && $min_year!=0;$i++){
				?>
                        		<option class="date_entry" value="<?php print $i; ?>" <?php if($max_flag && $max_year_filter==$i) print 'selected'; ?>><?php print $i; ?></option>
				<?php
			} ?>
                    </select>
                    
                    <div class="dropdown" style="margin-left:30px">
                        <div>
                            <div class="research_area_box">Authors
                                <div class="arrow_down"></div>
                            </div>
                        </div>
                        
                        <div class="research_area_checklist" id="author_filter">
                            <div class="research_area_list_entry tick">
                                <div class="untick_all">
                                    <a class="research_tick_box" style="cursor:pointer; text-decoration:underline;" id="untick_all_authors" onclick="untick_authors()">Untick All</a>
                                </div>
                                <div class="tick_all">
                                    <a class="research_tick_box" style="cursor:pointer; text-decoration:underline;" id="tick_all_authors" onclick="tick_authors()">Tick All</a>
                                </div>
                            </div>
			    <?php for($i=0;$i<mysql_num_rows($result_authors);$i++){ $row=mysql_fetch_row($result_authors); ?>
                            <div class="research_area_list_entry">
                                <input type="checkbox" class="research_tick_box" id="<?php print $row[0]; ?>" <?php if ($author_filter==false || in_array($row[0], $author_list))print 'checked'; ?>>
                                <label for="<?php print $row[0]; ?>" class="box_label"><?php print $row[0]; ?></label>
                            </div>
			    <?php } ?>
                        </div>
                    </div>
		    
                    <div class="submit_button" style="display:inline-block;width:250px;margin-left:30px; padding:3px;" onclick="apply_filter();">Filter</div>
                    <br>
                </div>

                <div id="research_areas_container" style="margin-top: 10px">
		    <?php			
			for($i=0;$i<$count;$i++){
				$row=mysql_fetch_row($result);
				if($area_filter && !in_array($row[8], $area_list))
					continue;
				$year=intval($row[3]);
				if($min_flag && $year < $min_year_filter)
					continue;
				if($max_flag && $year > $max_year_filter)
					continue;

	            $author_names = explode('|',$row[5]);
				$author_count = count($author_names);
				if($author_filter){
					$found=false;
					for($j=0;$j<$author_count;$j++){
						if(in_array($author_names[$j], $author_list)){
							$found=true;
							break;
						}
					}
					if (!$found){
						continue;
					}
				}	
	            $author_users = explode('|',$row[6]);
                    ?>
                        <div class="h_separator"></div>
                        <div class="research_area">
                                <div class="paper_title">
                                        <a href="pub.php?p_id=<?php print $row[0]; ?>"><?php print $row[1]; ?></a>
                                </div>
                                <div class="author_names">
                                <?php for($j=0;$j<$author_count;$j++){ ?>
                                        <?php if ($author_users[$j]!="") {?>
                                        <a href="profile.php?id=<?php print $author_users[$j]; ?>" class="author_name"><?php print $author_names[$j]; ?></a>
                                        <?php } else { print $author_names[$j]; } ?>
                                        <?php if ($j!=$author_count-1)print '|'; ?>
                                <?php } ?>
                                </div>
                                <div class="conference_journal_name">
                                        <?php print $row[2]; ?> (<?php print $row[3]; ?>)
                                </div>
                        </div>
            <?php } ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html> 
