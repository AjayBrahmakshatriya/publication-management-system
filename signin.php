<?php
	session_start();
	if (isset($_SESSION["master"]) && $_SESSION["master"]!=""){
		print '<html><head><script>window.location.href=".";</script></head></html>';
		return;
	}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<title>Sign In</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/signIn.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
<script>
signin_func=function(){
	username = document.getElementById("signin_username").value;
	password = document.getElementById("signin_password").value;
	captcha_response = document.getElementById("g-recaptcha-response").value;

	if(username=="" || password==""){
		document.getElementById("signin_response").innerHTML="Invalid username/password";
		return false;
	} else if(captcha_response=="") {
    document.getElementById("signin_response").innerHTML="Please enter captcha";
    return false;
	}else{
		document.getElementById("signin_response").innerHTML="";
		document.getElementById("signin_button").innerHTML="Authenticating...";
		$.post("backend/?method=2", {"username":username, "password":password, "captcha-response":captcha_response}).done(function(resp){
			console.log(resp);
			if (resp["code"] != 200){
				document.getElementById("signin_response").innerHTML=resp["message"];
				document.getElementById("signin_button").innerHTML="Sign In";
				return;
			}else{
				if (resp["first_login"]){
					window.location.href="edit_profile.php";
					return;
				}
				<?php if (isset($_GET["redirect"]) && $_GET["redirect"]!=""){?>
					window.location.href="<?php print $_GET["redirect"]?>";
				<?php }else{?>
					window.location.href=".";
				<?php } ?>
				
			}
		});
		return false;
	}

}
</script>
</head>

<body style="max-width:978px; margin:auto; ">

    <div id='title_bar'>
        <div id='title_text'>
            <a href=".">research@cse.iith.ac.in</a>
        </div>
    </div>
    <div class="sign_box">
        <img src="images/user.png" class="user_image">
        <form onsubmit="return signin_func();">
        <input id="signin_username" type="text" class="sign_in" placeholder="Enter username"/>
        <input id="signin_password" type="password" class="sign_in" placeholder="Enter password"/>
        <div class="g-recaptcha" data-sitekey="6LfU-CITAAAAAO9zaPq4Fx5HE_nrnOKH7-CMrFZd"></div>
        <button type="submit" id="signin_button" class="sign_in_button" >
            Sign In
        </button>
        </form>        
    	<div id="signin_response"></div>
	</div>
</body>
</html> 
