<?php
	require_once 'connect.php';
	require_once 'escape_get_post.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<title>Publications@cse.iith</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link href=
    '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic|Product+Sans:400'
    rel='stylesheet' type='text/css'>
</head>

<body style="max-width:978px; margin:auto; ">
	<div id='outline'>
	<?php require_once 'title.php';?>
		<br>
		<div id="the_body">
			<div class="body_divs main_left_box">
			<!-- CSE department, IITH likes to do research... Placeholder text for department research motto? -->

		</div>
			<div class="body_divs" id="research_areas" >
				<div style="margin-top:30px; font-size:30px; text-align: left; margin-left: 10px">
					Site Contributors
				</div>
				<div id="site_contributors">
					

<p>The first version of this site has been developed by</p>

Ajay Brahmakshatriya [cs12b1004@iith.ac.in]<br>
Pratik Bhatu [cs12b1010@iith.ac.in]<br>
Samrat Phatale [cs12b1035@iith.ac.in]<br>
Siddhant Agarwal [es12b1018@iith.ac.in]<br>

<p>
All are final year B.Tech students from the Department of Computer Science & Engineering at IIT Hyderabad in April, 2016.
</p>
				</div>
			</div>
		</div>
	</div>
    	<?php require_once 'footer.php';?>
<div id="wrap">

	<div id="main">
<hr>
<p>		<ul id="navigationFooter">
               
                <li>
                    <a href="siteContributors.php">Site Contributors</a>
                </li>
                  </ul></p>
	</div>

</div>
	
</body>
</html> 
