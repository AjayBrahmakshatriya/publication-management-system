<?php
	require_once 'escape_get_post.php';
	if(!isset($_SESSION)) 
	{ 
		session_start(); 
	} 
	if (isset($_SESSION["master"]) && $_SESSION["master"]!=""){
                $logged_in = true;
                $username = $_SESSION["master"];
                $profile_name = $_SESSION["name"];
        }else{
                $logged_in=false;
        }
	$url =  urlencode("//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
?>
                
                <div id='title_bar'>
                    <div id='title_text'>
                            <a href=".">research@cse.iith.ac.in</a>
                    </div>
                </div>

                <div id = "search_bar_container">
                        <div id="search_bar">
				<form action="search_results.php" method="GET" style="float:left">
                                <input type="text" name ="search_query" class="search_box" placeholder=" Search" <?php if(isset($_GET["search_query"]) && $_GET["search_query"]!=""){print 'value="'.$_GET["search_query"].'"';} ?>/>
                                <button type="submit" class="search_button" ></button>
				</form>
                                <?php if($logged_in && $_SESSION["role"]=="faculty"){
                                        ?><div style="float:left;line-height:30px;font-size:20px;">&nbsp;|&nbsp;<a href="add_paper.php">Add paper</a></div><?php
                                }?>
                        </div>

                        <div class="sign_in_link">
                                <?php if($logged_in){?><a href="profile.php?id=<?php print $username;?>"> 
                                <?php print $profile_name;?></a>
                                <a href="edit_profile.php"><img src="images/edit_profile.png" width="25px" height="25px" style="display: inline-block;margin-left:4px;margin-bottom:-5px;"></a>
                                | <a href="signout.php">Sign out</a>
                                
                                <?php }else{?> <a href="signin.php?redirect=<?php print $url?>">Sign In</a><?php }?>

                        </div>
                </div>